/** Thymeleaf custom variables */
window.vars = {
  baseDomain: document.location.origin,
  webroot: /*[[${backend_webroot}]]*/ '/',
  tenant: /*[[${tenant}]]*/ 'ES',
  editURL: /*[[${backend_edit}]]*/ 'http://localhost:8888/template/edit',
  saveURL: /*[[${backend_save_url}]]*/ 'http://localhost:8888/save',
  newURL: /*[[${backend_new_url}]]*/ 'http://localhost:8888/new',
  removeURL: /*[[${backend_remove_url}]]*/ 'http://localhost:8888/remove',
  editorURL: /*[[${backend_editor_url}]]*/ 'http://localhost:8888/editor',
  mainURL: /*[[${backend_main_url}]]*/ 'http://localhost:8888/main',
  logout: /*[[${backend_logout_url}]]*/ 'http://localhost:8888/logout',
  sessionId: /*[[${backend_sm_session_id}]]*/ 'example_session_id'
};

var editor; // use a global for the submit and return data rendering in the examples

$(document).ready(function() {
  editor = new $.fn.dataTable.Editor( {
    ajax: {
      create: {
        type: 'POST',
        url:  '../php/rest/create.php'
      },
      edit: {
        type: 'PUT',
        url:  '../php/rest/edit.php?id=_id_'
      },
      remove: {
        type: 'DELETE',
        url:  '../php/rest/remove.php?id=_id_'
      }
    },
    table: "#codes",
    fields: [ {
      label: "First name:",
      name: "first_name"
    }, {
      label: "Last name:",
      name: "last_name"
    }, {
      label: "Position:",
      name: "position"
    }, {
      label: "Office:",
      name: "office"
    }, {
      label: "Extension:",
      name: "extn"
    }, {
      label: "Start date:",
      name: "start_date"
    }, {
      label: "Salary:",
      name: "salary"
    }
    ]
  } );

  $('#codes').DataTable( {
    dom: "Bfrtip",
    ajax: "../php/rest/get.php",
    columns: [
      { data: null, render: function ( data, type, row ) {
          // Combine the first and last names into a single table field
          return data.first_name+' '+data.last_name;
        } },
      { data: "position" },
      { data: "office" },
      { data: "extn" },
      { data: "start_date" },
      { data: "salary", render: $.fn.dataTable.render.number( ',', '.', 0, '$' ) }
    ],
    select: true,
    buttons: [
      { extend: "create", editor: editor },
      { extend: "edit",   editor: editor },
      { extend: "remove", editor: editor }
    ]
  });

});

