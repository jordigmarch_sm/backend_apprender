<img src="http://www.grupo-sm.com/sites/all/themes/sm/img/logo-sm.png" />

**:: Generic Backend Server ::**

=======

****
Generic Backend Server with real time capabilities, well built versioning|history
system, scalable/stateless, ....

## Installation

Standalone & autoboxed. Everything from static resources, local file upload/downloading, 
atmosphere comet server + REST services is served directly from the main entry point without
further dependencies. 

## Dependencies

* Linux OS. x86_64 preferred & Kernel >= 3.x recommended.
* JVM 8, needed because of Jetty 9 server depends on Java 8.
* ImageMagick, for PDF thumbnail generation.
* LibreOffice/OpenOffice, for PDF conversion (thus thumbnailing) of any possible file.
* FFMPEG with ffprobe, for video transcoding + thumbnailing.
* id3lib, for MP3 ID3 tag querying.

## Issues/Notes

* Session Timeout is done at DDBB level, thus avoiding the need of session affinity in a HA cluster.
* Security & hacking control is done exclusively via salted checksumming. Single Sign On, 
security/administration parameters and so on are checked on server (database) for single
node operations security coherence. For instance:  

### JavaScript obfuscation/minification via 2 different stages: 
* viewer.min.js -> with all reusable code and components, via Google Closure Compiler at building
 (ANT) time
* index.html -> with the help of gulp minifyInine, which uses uglifyJS2, and with a special 
comment striping regex to keep Thymeleaf substitution at server side (regex = Everything that starts
with /\*\[  ---> **/^\/*\[/**)

### Gource git video generation

* gource -s 0.1 --viewport 1024x768 --title mindSMapper -o gource.ppm
* ffmpeg -y -r 60 -f image2pipe -vcodec ppm -i gource.ppm -vcodec libx264 -preset ultrafast -pix_fmt yuv420p -crf 1 -threads 0 -bf 0 gource.mp4

### i18n - Excel exporting

* cat messages_es.properties | iconv --from iso-8859-1 --to utf-8 | grep -v -e "^#.*$\|^$" > es.csv

