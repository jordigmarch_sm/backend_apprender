#! /bin/sh

###############################################################################
# mindSMapper Graphic Toolkit Server - Servidor de Organizadores Gráficos - SM
# -----------------------------------------------------------------------------
# 
# Copyright (C) 2016 
#  - Jordi G. March <jordi.garcia@colex.grupo-sm.com>
#  - Desarrollo Tecnologías Interactivas
#
# Versiones:
# ----------
# 1.0 - Versión inicial
###############################################################################

# JAVA_HOME / Memory Settings / Log Level [debug|info|warn|error|fatal]
JAVA_BIN="/bin/java"
# Favorecemos latencia/usuario vs backend (G1!)
JAVA_OPTS="-Xms256m -Xmx1560m -XX:+UseG1GC"
LOG_LEVEL=info
ULIMIT=65536

###############################################################################
#                       ---------------------------------
#                       ... NO TOCAR A PARTIR DE AQUÍ ...
#                       ---------------------------------
###############################################################################

# add the libraries to the MAPPER_CLASSPATH
# EXEDIR is the directory where this executable is.
EXEDIR=${PWD}

DIRLIBS=${EXEDIR}/lib/*.jar
for i in ${DIRLIBS}
do
  if [ -z "$MAPPER_CLASSPATH" ] ; then
    MAPPER_CLASSPATH=$i
  else
    MAPPER_CLASSPATH="$i":$MAPPER_CLASSPATH
  fi
done

DIRLIBS=${EXEDIR}/lib/*.zip
for i in ${DIRLIBS}
do
  if [ -z "$MAPPER_CLASSPATH" ] ; then
    MAPPER_CLASSPATH=$i
  else
    MAPPER_CLASSPATH="$i":$MAPPER_CLASSPATH
  fi
done

# Prependamos JAR principal
MAPPER_CLASSPATH=${EXEDIR}/lib/mapper-1.0.jar:$MAPPER_CLASSPATH

# Procesos para esta subshell...
#ulimit -n $ULIMIT
#ulimit -Hn $ULIMIT

#echo "CLASSPATH: $MAPPER_CLASSPATH"

# Otra opciones necesarias...
JAVA_OPTS="${JAVA_OPTS} -Dfile.encoding=UTF-8 -Dorg.eclipse.jetty.util.log.class=org.eclipse.jetty.util.log.StdErrLog"

$JAVA_BIN -classpath "$MAPPER_CLASSPATH:$CLASSPATH" \
	$JAVA_OPTS -DLOG_LEVEL=$LOG_LEVEL \
	-Dhazelcast.config=${EXEDIR}/config/hazelcast.conf \
	-Duser.dir=${EXEDIR} \
	com.gruposm.mapper.Main "$@" > stdout.log 2>&1
