package com.gruposm.backend.persistence.mappers;


/**
 * DataBase Creation Mapper
 *
 * @version 0.1
 * @date Jun 27, 2016
 * @author Jordi G. March <jordi.garcia@colex.grupo-sm.com>
 */
 public interface DBCreatorMapper {

	void createTenant();
	void createAdmin();
	void createSession();
	void createApp();
	void createAppCode();

	/** Drops */
	void dropAdmin();
	void dropSession();
	void dropTenant();
	void dropAppCode();
	void dropApp();

	/** Alters */

	/** Default Data */
	void addTenantData();
	void addAdminData();

	/** Foreign checks */
	void disableChecks();
	void enableChecks();

}
