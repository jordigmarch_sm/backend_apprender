package com.gruposm.backend.persistence.mappers;

import com.gruposm.backend.domain.App;

import java.util.List;

/**
 * App Mapper
 * 
 * @version 0.1
 * @date Nov 14, 2016
 * @author Jordi G. March <jordi.garcia@colex.grupo-sm.com>
 */
public interface AppMapper {

	/**
	 * Default Insert method,  
	 * @param app ...
	 */
	public void insert(App app);
	
	/**
	 * Default Update method,  
	 * @param app ...
	 */
	public void update(App app);
	
	/**
	 * Default...
	 * @return
	 */
	public List<App> getAll();
	
	/**
	 * Default...
	 * @return
	 */
	public void remove(App app);
	
}
