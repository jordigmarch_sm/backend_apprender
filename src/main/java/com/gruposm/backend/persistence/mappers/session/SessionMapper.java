package com.gruposm.backend.persistence.mappers.session;

import java.util.HashMap;
import java.util.List;

import com.gruposm.backend.domain.session.Session;


/**
 * Session Mapper
 * 
 * @version 0.1
 * @date Nov 15, 2016
 * @author Jordi G. March <jordi.garcia@colex.grupo-sm.com>
 */
public interface SessionMapper {

	/**
	 * Default Insert method,  
	 * @param session ...
	 */
	public void insert(Session session);
	
	/**
	 * Default Update method,  
	 * @param session ...
	 */
	public void update(Session session);
	
	/**
	 * Default...
	 * @return List<Session> ...
	 */
	public List<Session> getAll();
	
	/** 
	 * Get list of sessions associated to user/sessionId 
	 * (should be 0/1 ideally, but can be more in case of errors)
	 * 
	 * @param id of Session/Username
	 * @return List<Session> 
	 */
	public List<Session> getById(String id);

	/**
	 * Default...
	 * @return
	 */
	public void remove(Session Session);

	/**
	 * Remove sessions after specified minutes timeout
	 * @param minutes
	 */
	public void removeSessionsAfter(int minutes);

	/**
	 * Remove sessions after specified minutes timeout and which don't have a proper
	 * attribute in its <code>attributes</code> field
	 * @param map
	 */
	public void removeNormalSessionsAfter(HashMap<String, Object> map);

	/**
	 * Updates just timestamp and no attributes at all (basic touchSession)
	 * @param session
	 */
    void updateJustTimestamp(Session session);

	/**
	 * Gets session by user_id + tenant - Multitenant aware!
	 *
	 * @param map ...
	 * @return session ...
	 */
	Session getByUserId(HashMap<String, String> map);

	/**
	 * Get list of sessions associated to a given deviceId
	 * (should be 0/1 ideally, but can be more in case of errors)
	 *
	 * @param deviceId of iOS|Android physical device ...
	 * @return List<Session>
	 */
    List<Session> getByDeviceId(String deviceId);
}
