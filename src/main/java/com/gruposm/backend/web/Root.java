package com.gruposm.backend.web;

import com.gruposm.backend.utils.Config;
import com.gruposm.backend.utils.Constants;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet implementation for root|null contexts...
 *
 * Just sendRedirect to the main servlet we decide....
 */
@WebServlet("")
public class Root extends BaseServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor.
     */
    public Root() {
    }

	/**
	 * @see HttpServlet#!doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect(response.encodeRedirectURL(request.getContextPath()
				+ Config.getBackendWebroot() + Constants.MAIN_SERVLET));
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
