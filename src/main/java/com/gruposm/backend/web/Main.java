package com.gruposm.backend.web;

import com.gruposm.backend.exception.AuthException;
import com.gruposm.backend.utils.Config;
import com.gruposm.backend.utils.Constants;
import com.gruposm.backend.utils.thymeleaf.TemplateEngineUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet implementation for normal student requests...
 */
@WebServlet(Constants.MAIN_SERVLET)
public class Main extends BaseServlet {
	private static final long serialVersionUID = 1L;

	/** Main Page */
	private static final String MAIN_PAGE = "index";

    /**
     * Default constructor.
     */
    public Main() {
    }

	/**
	 * @see HttpServlet#!doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Prepare everything...
		prepareEverything(request, response);

		engine.process(MAIN_PAGE, ctx, response.getWriter());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	/**
	 */
	protected void prepareEverything(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Prepare response
		prepareResponse(request, response);

		// Check Auth
		try {
			// Base session - Common procedure...
			sessionId = authUtils.getSession(request, response);
			if (sessionId == null) {
				if (authUtils.isAdminRequest(request) && Config.isSessionRequired()) {
					String login = Config.getBackendWebroot() + Constants.LOGIN_SERVLET;
					response.sendRedirect(login);
				} else {
					sessionId = authUtils.validate(request, response);
				}
			}
		} catch (AuthException e) {
			throw new ServletException(e);
		}

		if (sessionId != null) {
			prepareTemplateContext(TemplateEngineUtil.TEMPLATE_MAIN);
		}
	}

}
