package com.gruposm.backend.web;

import com.gruposm.backend.exception.AuthException;
import com.gruposm.backend.utils.Constants;
import com.gruposm.backend.utils.thymeleaf.TemplateEngineUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet implementation class FirstServlet
 */
@WebServlet(Constants.LOGIN_SERVLET)
public class Login extends BaseServlet {
	private static final long serialVersionUID = 1L;

	/** Login Page */
	private static final String LOGIN_PAGE = "login";


    /**
     * Default constructor.
     */
    public Login() {
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			String sessionId = authUtils.getSession(request, response); 
			if (sessionId != null) {
				String forward = request.getQueryString() != null ? "?" + request.getQueryString() : "";
				String redirect = getMainServletURL(forward);
				response.sendRedirect(redirect);
			}
		} catch (AuthException e) {
			// Prepare response...
			prepareResponse(request, response);
			
			prepareTemplateContext(TemplateEngineUtil.TEMPLATE_ADMIN);

		    engine.process(LOGIN_PAGE, ctx, response.getWriter());
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// OK, process login/pass, create session, SHA-1, token/expiration, ...., and finally redirect.
		try {
			authUtils.validate(request, response);
			String forward = request.getQueryString() != null ? "?" + request.getQueryString() : "";
			String redirect = getMainServletURL(forward);
			//RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(redirect);
			//dispatcher.forward(request, response);
			response.sendRedirect(redirect);
		} catch (AuthException e) {
			response.sendError(HttpServletResponse.SC_BAD_REQUEST, e.getMessage());
		}
	}
	
}
