package com.gruposm.backend.web;

import com.gruposm.backend.exception.AuthException;
import com.gruposm.backend.utils.Config;
import com.gruposm.backend.utils.Constants;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet implementation class FirstServlet
 */
@WebServlet(Constants.LOGOUT_SERVLET)
public class Logout extends BaseServlet 
{
	private static final long serialVersionUID = 1L;
	
    /**
     * Default constructor. 
     */
    public Logout() {
    }
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			String sessionId = authUtils.getSession(request, response); 
			if (sessionId != null) {
				authUtils.invalidate(request, response);
			}
		} catch (AuthException e) {
			; // doing logout! No problems here....
		} finally {
			String login = Config.getBackendWebroot() + Constants.LOGIN_SERVLET;
			response.sendRedirect(login);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
