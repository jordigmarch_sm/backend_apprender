package com.gruposm.backend.web;

import com.gruposm.backend.utils.AuthUtils;
import com.gruposm.backend.utils.Config;
import com.gruposm.backend.utils.Constants;
import com.gruposm.backend.utils.thymeleaf.ContextProcessor;
import com.gruposm.backend.utils.thymeleaf.TemplateEngineUtil;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Base Servlet implementation...
 */
public abstract class BaseServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
	
	/** Auth Utils */
	protected AuthUtils authUtils;
	
	/** Session Id */
	protected String sessionId;
	
	/** Request & response objects */
	protected HttpServletRequest request;
	protected HttpServletResponse response;
	
	/** ThymeLeaf webContext */
	protected WebContext ctx;
	
	/** Thymelead Engine Processor */
	protected TemplateEngine engine;
	

    /**
     * Default constructor. 
     */
    public BaseServlet() {
        authUtils = AuthUtils.getInstance();
    }
    
    /**
     * Prepare response for all
     * @param response
     * @throws ServletException
     */
    protected void prepareResponse(HttpServletRequest request, HttpServletResponse response) throws ServletException {
    	this.request = request;
    	this.response = response;
    	
  		response.setContentType("text/html;charset=UTF-8");
  		response.setHeader("Pragma", "no-cache");
  		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
		response.setHeader("Vary", "*");
  		response.setDateHeader("Expires", 0);
  	}
    
    
    /**
     * Basic Template processing with the usual/needed attributes...
     * <p>
     * @see !REST Interfaces to look for final URLs. No dynamic, just use @Path and the like...
     * 
     * @param type ...
     */
    protected void prepareTemplateContext(String type) {
    	engine = TemplateEngineUtil.getTemplateEngine(request.getServletContext(), type);
		ctx = new WebContext(request, response, request.getServletContext(), request.getLocale());
		ContextProcessor processor = ContextProcessor.getInstance();
		
		processor.processBaseVariables(ctx, sessionId);
		processor.processUserAttribs(ctx, authUtils.getAttributes(sessionId));
		// i18n on demand
    	String language = request.getLocale().getLanguage();
    	String locale = request.getParameter(Constants.LOCALE);
    	if (locale != null) language = locale; // override via parameter...
		processor.processLocale(engine, language);

		// Add tenant information
		String tenant = "";
		try {
			tenant = authUtils.getTenantFromRequest(request);
		} catch (Exception ignore) {}
		ctx.setVariable(Constants.TENANT, tenant);
    }

    /**
     * Get variable of a context, so super classes can access the 
     * <code>prepareTemplateContext</code> method above...
     * 
     * @param var
     * @return String
     */
    protected String getContextVar(String var) {
    	return (String) ctx.getVariable(var); // By default all of our vars will be String
    }

	/**
	 * Gets the final URL once properly validated...
	 * @param queryString ...
	 * @return ...
	 */
	protected String getMainServletURL(String queryString) {
		return Config.getBackendWebroot() + Constants.MAIN_SERVLET + queryString;
	}

}
