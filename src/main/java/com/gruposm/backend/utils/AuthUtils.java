package com.gruposm.backend.utils;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseToken;
import com.gruposm.backend.domain.Admin;
import com.gruposm.backend.domain.Tenant;
import com.gruposm.backend.domain.session.Session;
import com.gruposm.backend.exception.AuthException;
import com.gruposm.backend.exception.CookieException;
import com.gruposm.backend.exception.UnauthorizedException;
import com.gruposm.backend.service.AdminService;
import com.gruposm.backend.service.TenantService;
import com.gruposm.backend.service.session.SessionService;
import com.gruposm.backend.utils.thymeleaf.LocaleUtils;
import org.apache.log4j.Logger;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URLDecoder;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * Auth Utils for authorization/authentication
 * 
 * @version 0.1
 * @date Jun 24, 2016
 * @author Jordi G. March <jordi.garcia@colex.grupo-sm.com>
 */
public class AuthUtils {

	/** Log4Java */
	private static final Logger log = Logger.getLogger(AuthUtils.class);
	
	/** Singleton */
	private static final AuthUtils instance;
	
	/** Clean sessions each minute... */
	private static final long SLEEP_CLEANER = 60000l;
	
	/** Thread cleaner */
	private Thread threadCleaner;
	private SessionCleaner sessionCleaner;
	
	/** LocaleUtils */
	private LocaleUtils i18n;
	
	static {
	    instance = new AuthUtils();
	}
	

	/**
	 * 
	 */
	public AuthUtils() {
		sessionCleaner = new SessionCleaner();
		threadCleaner = new Thread(sessionCleaner);
		threadCleaner.setName("SessionCleaner");
		threadCleaner.start();
		
		i18n = LocaleUtils.getInstance();
	}
	
	public static AuthUtils getInstance() {
		return instance;
	}

	/**
	 * Validate <code>request</code> viability. 
	 * <p>
	 * Can be of two different forms:
	 * <ul>
	 * <li>A request containing username & password, intended for admin purposes</li>
	 * <li>A request containing different parameters & a SHA1 token for checksumming</li>
	 * </ul>
	 * In case <code>request</code> is validated by any means, <code>request</code> will be 
	 * added to a dedicated SessionStore  
	 * 
	 * @param request
	 * @param response - In order to submit a logged cookie...
	 * @throws AuthException in case request cannot be validated
	 */
	public String validate(HttpServletRequest request, HttpServletResponse response) throws AuthException {
		if ((Config.isSessionRequired() && getAttribute(request, Constants.TOKEN) != null) ||
			getAttribute(request, Constants.USER_ID) != null) {
			checkSecurityToken(request);
		} else {
			validateAdmin(request);
		}
		return finallyAcceptRequest(request, response);
	}

	/**
	 * SecurityToken auth wrapper, so we can merge here our custom SSO based token URL and
	 * the one forwarded by App|Mobile devices with a Firebase token
	 *
	 * @param request ...
	 * @throws AuthException ...
	 */
	private void checkSecurityToken(HttpServletRequest request) throws AuthException {
		if (getAttribute(request, Constants.USER_ID) != null) {
			checkStdSecurityToken(request);
		}
	}

	/**
	 * In case no user_id is supplied at QueryString, we assume it is a Firebase token
	 * login, so we will check against Firebase servers if the token supplied is valid.
	 *
	 * @param request ...
	 * @throws AuthException ...
	 */
	private void checkFirebaseSecurityToken(HttpServletRequest request) throws AuthException {
		String idToken = getAttribute(request, Constants.TOKEN);
		String deviceId = getAttribute(request, Constants.DEVICE_ID);
		try {
			boolean checkRevoked = true;
			FirebaseToken fbToken = FirebaseAuth.getInstance().verifyIdTokenAsync(idToken, checkRevoked).get();
			// Token is valid and not revoked.
			// Tengo que darle una sesión válida de usuario dentro de nuestro entorno, siendo:
			// user_id = decodedToken.getEmail()
			// fname = decodedToken.getName()
			// lname = ""
			// ...
			request.setAttribute(Constants.FROM_FIREBASE, "true");
			request.setAttribute(Constants.USER_ID, fbToken.getEmail());
			request.setAttribute(Constants.FNAME, fbToken.getName());
			request.setAttribute(Constants.LNAME, "");
			request.setAttribute(Constants.PICTURE, fbToken.getPicture());
			request.setAttribute(Constants.USER_ROLE, Constants.ROLE_APP_USER);
			request.setAttribute(Constants.DEVICE_ID, deviceId);
		} catch (Exception e) {
			log.error("<checkFirebaseSecurityToken>: Error decoding token: " + e.toString());
			throw new AuthException(e.getMessage());
		}
	}

	/**
	 * A standard <code>request</code> should contain information about a uuid instance
	 * or assignment, a user_id, and many other aspects not covered here.
	 * Finally, it _must_ contain a token checksumming all the prior parameters contained
	 * in the proper <code>queryString</code>
	 *
	 * @param request ...
	 * @throws AuthException In case checksumming is incorrect
	 */
	private void checkStdSecurityToken(HttpServletRequest request) throws AuthException {
		String queryString = request.getQueryString();
		String checksum = getAttribute(request, Constants.TOKEN);
		// Limpio checksum del queryString... 
		queryString = queryString.replace("?token=" + checksum, "");
		queryString = queryString.replace("&token=" + checksum, "");
		String intChecksum = "";
		if (checksum != null && !checksum.equals("") && queryString != null && !queryString.equals("")) {
			intChecksum = Utils.getSHA1(queryString + Config.getSecuritySalt());	
		} else {
			if (Config.isSessionRequired()) {
				throw new UnauthorizedException(i18n.getString(request.getLocale(), "auth.void.checksum"));
			} else {
				// Security warning!! No checksum at all checked!
				checksum = intChecksum;
				log.warn("<checkSecurityToken>: Checksum omitted as per config! Please, use with caution!");
			}
		}

		// Control de timeout... (en segundos!)
		if (Config.isSecurityChecksumTimeoutEnabled()) {
			String strTimeout = getAttribute(request, Constants.TIMESTAMP_SHORT);
			if (strTimeout == null || strTimeout.equals("")) {
				log.warn("<checkSecurityToken>: SecurityChecksumTimeout enabled but no "
						+ Constants.TIMESTAMP_SHORT + " parameter passed!!");
				throw new UnauthorizedException(i18n.getString(request.getLocale(), "auth.provide.credentials"));
			}
			long when = Long.parseLong(strTimeout);
			long now = System.currentTimeMillis() / 1000;
			if (now - when > Config.getSecurityChecksumTimeout()) {
				log.warn("<checkSecurityToken>: SecurityChecksumTimeout surpassed: "
						+ (now - when) + " > " + Config.getSecurityChecksumTimeout() + " --> Unauthorized!");
				throw new UnauthorizedException(i18n.getString(request.getLocale(), "auth.provide.credentials"));
			}
		}

		// Right! Valid request -> Get username & store new Session....
		if (checksum.equals(intChecksum)) {
			String userId = getAttribute(request, Constants.USER_ID);
			if (userId == null || userId.equals("")) {
				throw new UnauthorizedException(i18n.getString(request.getLocale(), "auth.void.checksum", Constants.USER_ID));
			} else {
				log.info("<checkSecurityToken>: User " + userId + " validated properly... Logging in...");
			}
		} else {
			throw new UnauthorizedException(i18n.getString(request.getLocale(), "auth.invalid.checksum", checksum, queryString));
		}
	}

	/**
	 * Validates a given user properly authorized by Firebase ...
	 *
	 * @param request ...
	 * @throws AuthException ...
	 */
	private void validateFirebaseUser(HttpServletRequest request) throws AuthException {
		// Nothing else to do...
		// This method remains here just for future|possible convenience....

		// String fireBaseAuthToken = getAttribute(request, Constants.FIREBASE_AUTH_TOKEN);
		// ....
	}
	
	/**
	 * Validates a request with username + password ...
	 *
	 * @param request ...
	 * @throws AuthException ...
	 */
	private void validateAdmin(HttpServletRequest request) throws AuthException {
		String username = getAttribute(request, Constants.USERNAME);
		String password = getAttribute(request, Constants.PASSWORD);
		if (username == null || username.equals("") || password == null || password.equals("")) {
			throw new AuthException(i18n.getString(request.getLocale(), "auth.provide.credentials"));
		}
		AdminService as = new AdminService();
		Admin admin = as.getByName(username, getTenantFromRequest(request));
		if (admin == null) {
			throw new AuthException(i18n.getString(request.getLocale(), "auth.admin.notfound"));
		} else {
			if (admin.getPassword().equals(Utils.getSHA1(password))) {
				log.info("<validateAdmin>: " + username + " validated properly... Logging in...");
			} else {
				throw new AuthException(i18n.getString(request.getLocale(), "auth.passwords.notmatch"));
			}
		}
	}
	
	/**
	 * Once validated, clean sessions, insert and finally add cookie to response...
	 * 
	 * @param request
	 * @param response
	 * @return String sessionId
	 * @throws AuthException
	 */
	private String finallyAcceptRequest(HttpServletRequest request, HttpServletResponse response) throws AuthException {
		// Session ID 
		String sessionId = ""; 
		// OK, get username and add session...
		String userId = getUserIdentity(request);

		// This comes from Main|Login servlets, so it needs to be a _NEW_ session everytime!
		// Need attributes to persist into session and know where does this user belong to....
		sessionId = createSessionWithAttributes(request, userId);

		// Paste cookie...
		Cookie authCookie = new Cookie(Constants.SM_COOKIE_NAME, sessionId);
		// OJO!!!! En Edge se tiene que explicitar el path del backend webroot, además de el del
		// propio getPathInfo (que coge por defecto). El resto de navegadores cogen los dos sin
		// problemas, pero los chicos de Mocosoft siguen haciendo de las suyas... en fin...
		authCookie.setPath(Config.getBackendWebroot());
		response.addCookie(authCookie);
		authCookie.setPath(request.getPathInfo());
		response.addCookie(authCookie);
		log.info("<finallyAcceptRequest>: Cookie added for " + sessionId);
		
		return sessionId;
	}

	/**
	 * By Agreement: tenant will be the Fully Qualified Domain Name of the request!
	 *
	 * @see com.gruposm.backend.persistence.mappers.TenantMapper
	 * @param request ...
	 * @return String tenant (FQDN)
	 */
	public String getTenantFromRequest(HttpServletRequest request) throws AuthException {
		TenantService ts = new TenantService();
		Tenant tenant = ts.getByDomain(request.getServerName());
		if (tenant == null) {
			throw new AuthException("Tenant not available!");
		} else {
			return tenant.getTenant();
		}
	}

	/**
	 * 
	 * @param userId
	 * @return
	 * @throws AuthException
	 */
	private String createSessionWithAttributes(HttpServletRequest request, String userId) throws AuthException {
		String attributes = getAttributes(request);
		SessionService ss = new SessionService();
		String tenant = getTenantFromRequest(request);
		if (!attributes.contains(Constants.UUID) && !attributes.contains(Constants.FROM_FIREBASE)) {
			// OK, assume we're under admin login...
			AdminService as = new AdminService();
			Admin admin = as.getByName(userId, tenant);

			if (!attributes.contains(Constants.USER_ID)) {
				attributes += "&" + Constants.USER_ID + "=" + userId;
			}
			if (!attributes.contains(Constants.USER_ROLE)) {
				attributes += "&" + Constants.USER_ROLE + "=" + admin.getRole();
			}
			if (!attributes.contains(Constants.TIMESTAMP)) {
				attributes += "&" + Constants.TIMESTAMP + "=" + Math.toIntExact(System.currentTimeMillis()/1000);
			}
			if (!attributes.contains(Constants.FNAME)) {
				attributes += "&" + Constants.FNAME + "=" + admin.getFname();
			}
			if (!attributes.contains(Constants.LNAME)) {
				attributes += "&" + Constants.LNAME + "=" + admin.getLname();
			}
			// Add Locale
			String locale = request.getLocale().getLanguage();
			if (getAttribute(request, Constants.LOCALE) != null) {
				locale = getAttribute(request, Constants.LOCALE);
			}
			attributes += "&" + Constants.LOCALE + "=" + locale;
			// Check if we need to invalidate Locale cache per userId|sessionId
			String prevLocaleForUser = LocaleUtils.getInstance().getLocaleFor(userId, tenant);
			if (prevLocaleForUser != null && !prevLocaleForUser.equals(locale)) {
				LocaleUtils.getInstance().flushUserLocaleCache();
			}
		}
		return ss.insert(userId, attributes, tenant);
	}

	public void addToSessionAttributes(String sessionId, String attribKey, String attribVal) {
		SessionService ss = new SessionService();
		Session session = ss.getById(sessionId);
		String attributes = session.getAttributes();
		if (!attributes.contains(attribKey + "=" + attribVal)) {
			attributes += "&" + attribKey + "=" + attribVal;
			session.setAttributes(attributes);
			ss.update(session);
		}
	}

	/**
	 * Checks if this <code>request</code> is logged.
	 * <p>
	 * <b>PRIORITIES:</b>
	 * <ul>
	 * <li>First: vía username in a GET request</li>
	 * <li>Second (or last): vía cookie</li>
	 * </ul>
	 * In case multiple users are interacting in the same browser, session must be 
	 * evaluated based on the default GET method: board?user_id=XXX...., and not via
	 * cookie, as it may lead to confusion...
	 * 
	 * @param request
	 * @param response 
	 * @return String sessionId
	 * @return
	 */
	public String getSession(HttpServletRequest request, HttpServletResponse response) throws AuthException {
		// 1st prio = USER!
		String userId = getUserIdentity(request);
		String sessionId = null;
		if (request.getCookies() != null) {
			for (Cookie cookie: request.getCookies()) {
				if (cookie.getName().equals(Constants.SM_COOKIE_NAME)) {
					sessionId = cookie.getValue();
					break;
				}
			}
		}

		// Redefine cookie in case a user is submitted via parameter...
		if (userId != null && !userId.equals("")) {
			if (sessionId != null) {
				invalidate(request, response);
				sessionId = getOrCreateSession(userId, request);
			}
		}

		SessionService ss = new SessionService();
		if (sessionId == null) {
			String attributes = request.getQueryString();
			if (attributes == null || attributes.equals("")) {
				throw new AuthException(i18n.getString(request.getLocale(), "auth.session.notfound"));
			} else {
				// sessionId = createSessionWithAttributes(request, userId, attributes);
				sessionId = getOrCreateSession(userId, request);
			}
		} else {
			Session session = ss.getById(sessionId);
			if (session != null) {
				touchSession(session, request);
			} else {
				// Session expired -> Fallback to user...
				invalidate(request, response);
				throw new AuthException(i18n.getString(request.getLocale(), "auth.session.notfoundstorage"));
			}
		}

		log.debug("<getSession>: SessionId = " + sessionId);

		// Update cookie...
		Cookie authCookie = new Cookie(Constants.SM_COOKIE_NAME, sessionId);
		response.addCookie(authCookie);
		log.info("<getSession>: Cookie added for " + sessionId);
		
		return sessionId;
	}
	
	/**
	 * Checks against internal SessionStore (database for HA) if request is already logged.
	 * <p>
	 * This method will create a session on-the-fly in case it doesn't exist in DataBase,
	 * as it assumes a Single Sign On is currently on the go...
	 * 
	 * @param request
	 * @return String sessionId
	 * @return
	 */
	private String getOrCreateSession(String userId, HttpServletRequest request) throws AuthException {
		if (userId == null || userId.equals("")) {
			return null;
		} else {
			// Prior to create on-the-fly, check security token in case it is supplied...
			checkSecurityToken(request);
			
			SessionService ss = new SessionService();
			List<Session> sessions = ss.getSessionsById(userId);
			if (sessions == null || sessions.size() == 0) {
				log.info("<getSession>: No session found for '" + userId + "' Creating on-the-fly...");
				String sessionId = createSessionWithAttributes(request, userId);
				return sessionId;
			} else if (sessions.size() > 1) {
				log.info("<getSession>: Multiple session found for '" + userId + "' Aborting...");
				throw new AuthException(i18n.getString(request.getLocale(), "auth.session.multiplefounduser", userId));
			} else {
				Session session = sessions.get(0);
				touchSession(session, request);
				return session.getSession();
			}
		}
	}
	
	/**
	 * Touch Session - Log + update in Session Store...
	 * <p>
	 * Controls wether this session has been more than allowed time under 
	 * <code>security.session.timeout.mins</code>, throwing an <code>AuthException</code>
	 * in such case...
	 * 
	 * @param session
	 * @throws AuthException in case existing session is longer than timeout.mins...
	 */
	private void touchSession(Session session, HttpServletRequest request) throws AuthException {
		long lastUpdated = session.getUpdatedOn();
		long now = System.currentTimeMillis() / 1000;
		Date lastLogin = new Date(session.getCreatedOn() * 1000);
	    Format format = new SimpleDateFormat(Utils.DATE_FORMAT_LONG);
	    String loggedOn = format.format(lastLogin);
	    int ago = Math.toIntExact(now - lastUpdated);
		log.info("<touchSession>: User '" + session.getUserId() + "' updated session "
			+ ago + " seconds ago. Logged on '" + loggedOn + "'");
		SessionService ss = new SessionService();
		if (ago > Config.getSessionTimeout() * 60) {
			ss.remove(session);
			log.warn("<touchSession>: User '" + session.getUserId() + " didn't update session "
				+ "in too much time... Removing session - Next time it will be invalidated...");
			throw new AuthException(i18n.getString(request.getLocale(), "auth.session.expired"));
		} else {
			String attributes = session.getAttributes();
			String newAttribs = request.getQueryString();
			if (newAttribs != null && !newAttribs.equals("")) {
				String oldAttribs = session.getAttributes();
				attributes = consolidateAttributes(oldAttribs, newAttribs);
			}
			if (attributes != null &&  !attributes.equals("")) {
				session.setAttributes(attributes);
			}
			ss.update(session);
		}
	}


	/**
	 * Touch Session - Log + update in Session Store...
	 * <p>
	 * Controls wether this session has been more than allowed time under
	 * <code>security.session.timeout.mins</code>, throwing an <code>AuthException</code>
	 * in such case...
	 *
	 * @param session
	 * @throws AuthException in case existing session is longer than timeout.mins...
	 */
	private void touchSession(Session session) throws AuthException {
		long lastUpdated = session.getUpdatedOn();
		long now = System.currentTimeMillis() / 1000;
		Date lastLogin = new Date(session.getCreatedOn() * 1000);
		Format format = new SimpleDateFormat(Utils.DATE_FORMAT_LONG);
		String loggedOn = format.format(lastLogin);
		int ago = Math.toIntExact(now - lastUpdated);
		log.info("<touchSession>: User '" + session.getUserId() + "' updated session "
				+ ago + " seconds ago. Logged on '" + loggedOn + "'");
		SessionService ss = new SessionService();
		if (ago > Config.getSessionTimeout() * 60) {
			ss.remove(session);
			log.warn("<touchSession>: User '" + session.getUserId() + " didn't update session "
					+ "in too much time... Removing session - Next time it will be invalidated...");
			throw new AuthException(i18n.getString(Config.getDefaultLocale(), "auth.session.expired"));
		} else {
			ss.updateJustTimestamp(session);
		}

		// Add callStack so we can trace where we're spending time...
		Throwable t = new Throwable();
		StackTraceElement[] elements = t.getStackTrace();
		//String calleeMethod = elements[0].getMethodName();
		String stackTrace = "";
		for (int i = 1; i <= 5; i++) {
			String callerMethodName = elements[i].getMethodName();
			String callerClassName = elements[i].getClassName();
			stackTrace += callerClassName + "." + callerMethodName + "()" + " <-- ";
		}
		stackTrace = stackTrace.substring(0, stackTrace.length() - 4);
		log.debug("<touchSession>: from " + stackTrace);
	}

	/**
	 * Just in the case where cookies are not yet ready (syntactic board|mural exception, ...)
	 *
	 * @param sessionId ...
	 */
	public void invalidateSession(String sessionId) {
		if (sessionId != null && !sessionId.equals("")) {
			Session session = new Session();
			session.setSession(sessionId);
			SessionService ss = new SessionService();
			ss.remove(session);
			log.info("<invalidate>: Session " + sessionId + " invalidated!");
		}
	}

	/**
	 * Invalidate session based on Cookie...
	 * @param request
	 */
	public void invalidate(HttpServletRequest request, HttpServletResponse response) {
		String sessionId = invalidateCookie(request, response);
		invalidateSession(sessionId);
	}
	
	/**
	 * Invalidate Session Cookie associated to this request...
	 * <p>
	 *     <b>Don't use separately!! Use always <code>invalidate</code> instead!</b>
	 * </p>
	 *
	 * @param request ...
	 * @param response ...
	 * @return String sessionId
	 */
	private String invalidateCookie(HttpServletRequest request, HttpServletResponse response) {
		String sessionId = null;
		if (request.getCookies() != null) {
			for (Cookie cookie : request.getCookies()) {
				String name = cookie.getName();
				if (name.equals(Constants.SM_COOKIE_NAME)) {
					sessionId = cookie.getValue();
					cookie.setValue("");
					cookie.setMaxAge(0);
					response.addCookie(cookie);
					// Don't break here! There may exist more than 1 cookie with same name! (@see Edge...)
				}
			}
			log.info("<invalidateCookie>: Cookie for " + sessionId + " invalidated properly!");
		}
		return sessionId;
	}

	/**
	 * As there exists 2 methods for accessing a uuid (via Admin/Main), we will use a
	 * single <code>getUserIdentity</code> and base the rest of our code on <b>userId</b>.
	 * 
	 * @param request
	 * @return
	 */
	private String getUserIdentity(HttpServletRequest request) {
		String userId = getAttribute(request, Constants.USER_ID);
		if (userId == null || userId.equals("")) {
			// In case it is an admin request....
			userId = getAttribute(request, Constants.USERNAME);
			try {
				userId = userId.toLowerCase();
			} catch (Exception ignore) {}
		}
		return userId;
	}

	/**
	 * Checks if <code>request</code> comes from an Admin perspective. 
	 * <p>
	 * More precisely, as admin will be isolated from Moodle, this method will return <code>true</code>
	 * if <code>request</code> doesn't have a token checksum, which is what is used as a simple
	 * authorization/validation method for mere mortals.
	 *  
	 * @param request
	 * @return
	 */
	public boolean isAdminRequest(HttpServletRequest request) {
		return getAttribute(request, Constants.TOKEN) == null;
	}
	
	/**
	 * Get Attributes of a persisted sessionId/username
	 * 
	 * @param sessionId
	 * @return
	 */
	public HashMap<String, String> getAttributes(String sessionId) {
		HashMap<String, String> map = new HashMap<>();
		SessionService ss = new SessionService();
		List<Session> sessions = ss.getSessionsById(sessionId);
		if (sessions != null && sessions.size() == 1) {
			String attribs = sessions.get(0).getAttributes();
		    String[] pairs = attribs.split("&");
		    for (String pair : pairs) {
		        int idx = pair.indexOf("=");
		        try {
		        	map.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
		        } catch (Exception ignore) {}
		    }
		    return map;
		} else {
			return null;
		}
	}
	
	/**
	 * Get a concrete attribute of a persisted sessionId/username
	 * 
	 * @param sessionId
	 * @return
	 */
	public String getAttribute(String sessionId, String attribute) {
		HashMap<String, String> mapAttributes = getAttributes(sessionId);
		if (mapAttributes != null) {
			return mapAttributes.get(attribute);
		} else {
			return "";
		}
	}
	
	
	/**
	 * Consolidates two different session attributes into a single serialized String...
	 * 
	 * @param oldAttribs ...
	 * @param newAttribs ...
	 * @return A merged String containing attributes from both parameters
	 */
	private String consolidateAttributes(String oldAttribs, String newAttribs) {
		HashMap<String, String> mapOld = new HashMap<>();
	    String[] pairs = oldAttribs.split("&");
	    for (String pair : pairs) {
	        int idx = pair.indexOf("=");
	        try {
	        	mapOld.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
	        } catch (Exception ignore) {}
	    }
	    HashMap<String, String> mapNew = new HashMap<>();
	    pairs = newAttribs.split("&");
	    for (String pair : pairs) {
	        int idx = pair.indexOf("=");
	        try {
	        	mapNew.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
	        } catch (Exception ignore) {}
	    }
	    
	    // Merge both attribs in one single Map and serialize...
	    HashMap<String, String> resultMap = new HashMap<>();
	    resultMap.putAll(mapOld);
	    resultMap.putAll(mapNew);
	    String result = "";
	    for (String key : resultMap.keySet()) {
	    	result += key + "=" + resultMap.get(key) + "&";
	    }
	    return result.substring(0, result.length() - 1); // throw away last &
	}
	
	/**
	 * Checks for a valid sessionId stored in our persistent storage. 
	 * <p>
	 * Most basic one. Should be used with <code>MainEntity</code> instead for stronger security.
	 * 
	 * @param cookie
	 * @throws CookieException
	 */
	public void validateCookie(String cookie) throws CookieException, AuthException {
		SessionService ss = new SessionService();
		List<Session> sessions = ss.getSessionsById(cookie);
		if (sessions == null || sessions.size() == 0) {
			// No locale info here. Default to.... Config.getDefaultLocale()
			throw new CookieException(i18n.getString(Config.getDefaultLocale(), "auth.cookie.notfound"));
		} else if (sessions.size() > 1) {
			try {
				for (Session session : sessions) {
					ss.remove(session);
				}
			} catch (Exception ignore) {}
			throw new CookieException(i18n.getString(Config.getDefaultLocale(), "auth.cookie.invalidcount"));
		} else {
			; // Do nothing... Just don't throw anything...
			Session session = sessions.get(0);
			touchSession(session);
		}
	}
	
	/**
	 * Checks for a valid sessionId & admin interaction
	 * <p>
	 * Mainly for templates or other administration tasks, given the user must be admin|editor.
	 * Checks that role_user given to this cookie is one of editor|admin.
	 * <p>
	 * For hijacking purposes mainly....
	 * 
	 * @param cookie
	 * @throws CookieException
	 */
	public void validateAdminCookie(String cookie) throws CookieException, AuthException {
		// First -> Validate single Cookie...
		validateCookie(cookie);
		
		// Now, check this cookie belongs to editor|admin
		// Main Attribs...
		HashMap<String, String> mapAttribs = getAttributes(cookie);
		if (mapAttribs != null) {
			String userRole = mapAttribs.get(Constants.USER_ROLE);
			boolean ok = userRole.equals(Constants.ROLE_EDITOR) || userRole.equals(Constants.ROLE_ADMIN);
			if (!ok) {
				Analytics.getInstance().post(Analytics.CAT_HIJACK, "Admin Data Hijacking: " + mapAttribs.toString());
				throw new AuthException(i18n.getString(Config.getDefaultLocale(), "auth.cookie.hijack",
						cookie, mapAttribs.toString()));
			}
		} else {
			throw new AuthException(i18n.getString(Config.getDefaultLocale(), "auth.cookie.notvalid", cookie));
		}
	}

	/**
	 * Wrapper to query attributes|parameters from a <code>HttpServletRequest</code>
	 * object, so we can overload request with Attributes instead of base parameters...
	 *
	 * @param request ...
	 * @param attribute ...
	 */
	private String getAttribute(HttpServletRequest request, String attribute) {
		Object attr = request.getAttribute(attribute);
		if (attr != null && attr instanceof String) {
			return (String) attr;
		} else {
			return request.getParameter(attribute);
		}
	}

	/**
	 * Gets a String with all attributes of a <code>HttpServletRequest</code> object,
	 * separated by & and in key=value form.
	 * Includes both queryString parameters as well as Attributes (String) objects
	 *
	 * @param request ...
	 * @return String in the form key=value&key2=value2&...
	 */
	private String getAttributes(HttpServletRequest request) {
		List<String> listAttribs = new ArrayList<>();
		String attributes = request.getQueryString();
		if (attributes == null) attributes = "";
		// asList is immutable!
		listAttribs = new ArrayList<>(Arrays.asList(attributes.split("&")));

		// Overload with HttpServletRequest attributes
		Enumeration attrs =  request.getAttributeNames();
		while (attrs.hasMoreElements()) {
			String key = (String) attrs.nextElement();
			Object attr = request.getAttribute(key);
			if (attr != null && attr instanceof String) {
				String val = key + "=" + attr.toString();
				if (!listAttribs.contains(val)) {
					listAttribs.add(val);
				}
			}
		}

		// Serialize to store under session table...
		attributes = String.join("&", listAttribs);
		return attributes;
	}

	/**
	 * As a means of easy validation....
	 *
	 * @param id
	 * @param token
	 */
    public void validateToken(String id, String token) throws AuthException {
		String sha1sum = Utils.getSHA1(id + Config.getSecuritySalt());
		System.out.println("sha1sum = " + sha1sum + " -- " + id + Config.getSecuritySalt());
		if (!sha1sum.equals(token)) {
			throw new AuthException(i18n.getString(Config.getDefaultLocale(),
					"auth.invalid.checksum", token));
		}
    }

    /**
	 * A class implementing the Runnable interface to make proper session cleaning
	 * of the internal store on a minute basis.
	 * 
	 * @version 0.1
	 * @date Nov 15, 2016
	 * @author Jordi G. March <jordi.garcia@colex.grupo-sm.com>
	 */
	private class SessionCleaner implements Runnable {
		private int sessionTimeout = Config.getSessionTimeout();
		@Override
		public void run() {
			log.info("<run>: Starting SessionCleaner thread...");
			while (true) {
				try {
					Thread.sleep(SLEEP_CLEANER);
				} catch (Exception e) {
					log.error("<SessionCleaner>: Session Cleaning Daemon died!: " + e.toString());
					e.printStackTrace();
				}
				SessionService ss = new SessionService();
				ss.removeSessionsAfter(sessionTimeout);
			}
		}
	}
}