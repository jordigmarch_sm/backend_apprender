package com.gruposm.backend.utils;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

/**
 * Simple Inflater|Deflater wrapper for ZLib standard compression
 *
 * @version 0.1
 * @date Jan 16, 2018
 * @author Jordi G. March <jordi.garcia@colex.grupo-sm.com>
 */
public class Compressor {

    /** Singleton */
    private static final Compressor instance;
    static {
        instance = new Compressor();
    }

    private Compressor() {
    }

    public static Compressor getInstance() {
        return instance;
    }

    /**
     * Zlib Compress
     *
     * @param bytesToCompress byte[] array ...
     * @return byte[] ...
     */
    public byte[] compress(byte[] bytesToCompress) {
        Deflater deflater = new Deflater();
        deflater.setInput(bytesToCompress);
        deflater.finish();

        byte[] bytesCompressed = new byte[Short.MAX_VALUE];
        int numberOfBytesAfterCompression = deflater.deflate(bytesCompressed);
        byte[] returnValues = new byte[numberOfBytesAfterCompression];
        System.arraycopy(bytesCompressed, 0, returnValues, 0, numberOfBytesAfterCompression);
        return returnValues;
    }

    /**
     * Zlib Compress
     *
     * @param stringToCompress String ...
     * @return byte[] ...
     */
    public byte[] compress(String stringToCompress) {
        byte[] returnValues = null;
        try {
            returnValues = this.compress(stringToCompress.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException uee) {
            uee.printStackTrace();
        }

        return returnValues;
    }

    /**
     * Zlib Decompress
     *
     * @param bytesToDecompress byte[] ...
     * @return byte[] ...
     */
    public byte[] decompress(byte[] bytesToDecompress) {
        byte[] returnValues = null;

        Inflater inflater = new Inflater();
        int numberOfBytesToDecompress = bytesToDecompress.length;
        inflater.setInput(bytesToDecompress, 0, numberOfBytesToDecompress);
        int numberOfBytesDecompressedSoFar = 0;
        List<Byte> bytesDecompressedSoFar = new ArrayList<Byte>();
        try {
            while (!inflater.needsInput()) {
                byte[] bytesDecompressedBuffer = new byte[numberOfBytesToDecompress];
                int numberOfBytesDecompressedThisTime = inflater.inflate(bytesDecompressedBuffer);
                numberOfBytesDecompressedSoFar += numberOfBytesDecompressedThisTime;
                for (int b = 0; b < numberOfBytesDecompressedThisTime; b++) {
                    bytesDecompressedSoFar.add(bytesDecompressedBuffer[b]);
                }
            }

            returnValues = new byte[bytesDecompressedSoFar.size()];
            for (int b = 0; b < returnValues.length; b++) {
                returnValues[b] = (byte) (bytesDecompressedSoFar.get(b));
            }
        } catch (DataFormatException dfe) {
            dfe.printStackTrace();
        }
        inflater.end();
        return returnValues;
    }

    /**
     * Zlib Decompress
     *
     * @param bytesToDecompress byte[] ...
     * @return String ...
     */
    public String decompressToString(byte[] bytesToDecompress) {
        byte[] bytesDecompressed = this.decompress(bytesToDecompress);

        String returnValue = null;
        try {
            returnValue = new String(bytesDecompressed, 0, bytesDecompressed.length, "UTF-8");
        } catch (UnsupportedEncodingException uee) {
            uee.printStackTrace();
        }
        return returnValue;
    }
}