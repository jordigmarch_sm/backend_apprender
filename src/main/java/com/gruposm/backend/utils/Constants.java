package com.gruposm.backend.utils;

/**
 * Constants class. 
 * <p>
 * All parameters, IDs, .... should be accesed solely via this class...
 * 
 * @version 0.1
 * @date Jul 04, 2016
 * @author Jordi G. March <jordi.garcia@colex.grupo-sm.com>
 */
public class Constants
{
	/** Global attributes to be used here & in front-end servlets */
	public static final String UUID = "uuid";
    public static final String USER_ID = "user_id";
	public static final String USER_ROLE = "user_role";
	public static final String TIMESTAMP = "timestamp";
	public static final String USERNAME = "username";
	public static final String PASSWORD = "password";
	public static final String FNAME = "fname";
	public static final String LNAME = "lname";
	public static final String SESSION = "session";
	public static final String LOCALE = "locale";
	public static final String PICTURE = "picture";

	/** Token - Integrity checksum - Security */
	public static final String TOKEN = "token";
	public static final String TIMESTAMP_SHORT = "ts";

	/** Cookie Session ID */
	public static final String SM_COOKIE_NAME = "SMSESSIONID";

	/** Google Analytics */
	public static final String GOOGLE_ANALYTICS = "gaAccountId";
	
	/** Default values for roles */
	public static final String ROLE_STUDENT = "student";
	public static final String ROLE_TEACHER = "teacher";
	public static final String ROLE_ADMIN = "admin";
	public static final String ROLE_EDITOR = "editor";
	public static final Object ROLE_APP_USER = "appuser";

   	/** Cookie para fileDownload */
	public static final String FILE_DOWNLOAD_COOKIE_NAME = "fileDownload";

	/** Main Servlets (no prior slash!) */
	public static final String MAIN_SERVLET = "main";
	public static final String LOGIN_SERVLET = "login";
	public static final String LOGOUT_SERVLET = "logout";
	public static final String ATMOSPHERE_SERVLET = "collab";

	/** Constantes para Thymeleaf Templating */
	public static final String URL_LOGIN = "urlLogin";
	public static final String URL_MAIN = "urlMain";
	
	/** Thymeleaf - SlateBox URLs / Constants */
	public static final String BACKEND_SEARCH = "backend_search";
	public static final String BACKEND_WEBROOT = "backend_webroot";
	public static final String BACKEND_SAVE_URL = "backend_save_url";
	public static final String BACKEND_EXPORT_URL = "backend_export_url";
	public static final String BACKEND_GET_URL = "backend_get_url";
	public static final String BACKEND_ATMOSPHERE = "backend_atmosphere";
	public static final String BACKEND_IMAGE = "backend_image";
	public static final String BACKEND_VIDEO = "backend_video";
	public static final String BACKEND_UPLOAD = "backend_upload";
	public static final String BACKEND_MAX_UPLOAD_SIZE = "backend_max_upload_size";
	public static final String BACKEND_LOGOUT_URL = "backend_logout_url";
	public static final String BACKEND_SM_SESSION_ID = "backend_sm_session_id";
	public static final String BACKEND_SAVEDEVICE_URL = "backend_savedevice_url";

	/** Entidad principal por defecto */
	public static final String DEFAULT_ENTITY = "{...}";

	/** MultiTenancy */
	public static final String TENANT = "tenant";

	/** Control de errores en cliente */
    public static final String JS_FATAL_ERRORS_ARRAY = "js_fatal_errors_array";
	public static final String JS_FATAL_ERRORS_REPORT_ALL = "js_fatal_errors_report_all";

	/** Handlers para envío por Push vía FCM|APNS */
	public static final String PUSH_TYPE_ANDROID = "Android";
	public static final String PUSH_TYPE_IOS = "iOS";
    public static final String PUSH_TYPE_FIREBASE = "Firebase";
	public static final String NOTIFIED = "notified";

	/** Firebase Utils */
	public static final String FROM_FIREBASE = "from_firebase";
    public static final String DEVICE_ID = "device_id";

}