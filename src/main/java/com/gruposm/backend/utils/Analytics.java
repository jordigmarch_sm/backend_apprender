package com.gruposm.backend.utils;

import com.brsanthu.googleanalytics.AppViewHit;
import com.brsanthu.googleanalytics.EventHit;
import com.brsanthu.googleanalytics.GoogleAnalytics;
import org.apache.log4j.Logger;

/**
 * Google Analytics tracking system
 * 
 * @version 0.1
 * @date Jul 12, 2016
 * @author Jordi G. March <jordi.garcia@colex.grupo-sm.com>
 */
public class Analytics {

	/** Log4Java */
	private static final Logger log = Logger.getLogger(Analytics.class);
	
	/** GoogleAnalytics object */
	private GoogleAnalytics ga;
	
	/** Basic event categories */
	public static final String CAT_READ = "READ";
	public static final String CAT_WRITE = "WRITE";
	public static final String CAT_DELIVER = "DELIVER";
	public static final String CAT_HIJACK = "HIJACK";
	public static final String CAT_DEMO = "DEMO";
	

	/** Singleton */
	private static final Analytics instance;
	static {
		instance = new Analytics();
	}
	
	/**
	 * 
	 */
	public Analytics() {
		if (Config.isGAEnabled()) {
			ga = new GoogleAnalytics(Config.getGAaccountId());	
		} else {
			ga = null;
		}
	}
	
	public static Analytics getInstance() {
		return instance;
	}

	/**
	 * 
	 * @param description
	 */
	public void post(String description) {
		try {
			if (ga != null) {
				ga.postAsync(new AppViewHit(description));
				log.debug("<post>: GA posted AppViewHist with " + description);
			}
		} catch (Exception e) {
			log.warn("<post>: Unable to post GA trace: " + e.toString());
		}
	}
	
	/**
	 * 
	 * @param category
	 * @param action
	 */
	public void post(String category, String action) {
		try {
			if (ga != null) {
				ga.postAsync(new EventHit(category, action));
				log.debug("<post>: GA posted Eventhist [" + category + "] with " + action);
			}
		} catch (Exception e) {
			log.warn("<post>: Unable to post GA trace: " + e.toString());
		}
	}
	
}