package com.gruposm.backend.utils;

import com.gruposm.backend.domain.App;
import com.gruposm.backend.domain.AppCode;
import com.gruposm.backend.service.AppCodeService;
import com.gruposm.backend.service.AppService;
import org.apache.log4j.Logger;

import java.util.UUID;

/**
 * Utilidades para la generación de códigos...
 * 
 * @version 0.1
 * @date May 29, 2018
 * @author Jordi G. March <jordi.garcia@colex.grupo-sm.com>
 */
public class CodeUtils {

	/**
	 * Log4Java
	 */
	private static final Logger log = Logger.getLogger(CodeUtils.class);

	private static final CodeUtils instance;

	static {
		instance = new CodeUtils();
	}

	/**
	 *
	 */
	public CodeUtils() {
	}

	public static CodeUtils getInstance() {
		return instance;
	}

	/**
	 * Generates a random 6 digit code (caps), and checks against DDBB that the
	 * code returned has not been generated before.
	 *
	 * @return
	 */
	private String genRandomCode() {
		long ini = System.currentTimeMillis();
		boolean found = false;
		String code = "";
		while (!found) {
			UUID uid = UUID.randomUUID();
			code = uid.toString().substring(0, 6).toUpperCase();
			if (code.contains("0") || code.toLowerCase().contains("o")) {
				// just ignore...
			} else {
				AppCodeService acs = new AppCodeService();
				found = acs.getBy(code) == null;
			}
		}
		long time = System.currentTimeMillis() - ini;
		log.info("<genRandomCode>: New code [" + code + "] generated in " + time + " ms.");
		return code;
	}

	public void insertRaw(int idApp, String code) {
		AppCodeService acs = new AppCodeService();
		AppCode appCode = new AppCode();
		appCode.setIdApp(idApp);
		appCode.setUuidCode(code);
		appCode.setCreatedOn(System.currentTimeMillis());
		appCode.setUsedBy("");
		acs.insert(appCode);
	}

	/**
	 * Para generación bajo demanda según pidan Salva/MªJose....
	 */
	public void genRaw() {
		for (int i = 1; i <= 6; i++) {
			String[] apps = {"", "Artom", "Artric", "Arkim", "Arlant", "Arxy", "Crantal"};
			System.out.println(apps[i]);
			for (int j = 0; j < 600; j++) {
				String code = genRandomCode();
				insertRaw(i, code);
				System.out.println(code);
			}
			System.out.println("-----------------------------------------------------------");
		}
	}

	public static void main(String[] args) {
		// System.out.println("code = " + CodeUtils.getInstance().genRandomCode());
		CodeUtils.getInstance().genRaw();
		System.exit(0);
	}

}