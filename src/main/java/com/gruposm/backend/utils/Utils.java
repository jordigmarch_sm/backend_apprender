package com.gruposm.backend.utils;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.tika.Tika;
import org.apache.tika.config.TikaConfig;
import org.apache.tika.mime.MimeType;

import java.io.File;
import java.net.URLDecoder;
import java.text.DecimalFormat;
import java.text.Normalizer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * General utils...
 * 
 * @version 0.1
 * @date Jun 24, 2016
 * @author Jordi G. March <jordi.garcia@colex.grupo-sm.com>
 */
public class Utils {

	/** Log4Java */
	private static final Logger log = Logger.getLogger(Utils.class);

	/** Default Date Long Format */
	public static final String DATE_FORMAT_LONG = "dd/MM/yyyy HH:mm:ss";
	public static final String DATE_FORMAT_SHORT = "dd/MM/yyyy";
	public static final String DATE_FORMAT_ISO8601 = "yyyy/MM/dd HH:mm:ss";

	/** Singleton */
	private static final Utils instance;
	static {
		instance = new Utils();
	}

	/**
	 * 
	 */
	public Utils() {
	}

	public static Utils getInstance() {
		return instance;
	}

	/**
	 * Devuelve la codificación base64 del fichero (ruta a sistema de ficheros)
	 * pasado como parámetro.
	 * <p>
	 * En caso de que no se pueda realizar la codificación se devuelve la imagen
	 * original
	 * <p>
	 * Se utiliza Tika para determinar el contentType asociado a la imagen
	 * 
	 * @param imagePath
	 * @return
	 */
	public String getBase64Image(String imagePath) throws Exception {
		File f = new File(imagePath);
		String mimeType = getContentType(imagePath);
		String base64 = Base64.encodeBase64String(FileUtils.readFileToByteArray(f));
		return "data:" + mimeType + ";base64," + base64;
	}

	/**
	 * @see !getBase64Image without mimeType supplied...
	 * 
	 * @param imagePath
	 * @param mimeType
	 * @return
	 * @throws Exception
	 */
	public String getBase64Image(String imagePath, String mimeType) throws Exception {
		File f = new File(imagePath);
		String base64 = Base64.encodeBase64String(FileUtils.readFileToByteArray(f));
		return "data:" + mimeType + ";base64," + base64;
	}

	/**
	 * Get ContentType of a given path file
	 * 
	 * @param filePath ...
	 * @return a String rep. of mimeType
	 */
	public String getContentType(String filePath) {
		File f = new File(filePath);
		String mimeType = "image/png"; // en caso de error
		// TIKA para coger MimeType
		try {
			Tika tika = new Tika();
			mimeType = tika.detect(f);
		} catch (Exception ignore) {
		}

		return mimeType;
	}

	/**
	 * Obtiene la extensión por defecto de un tipo MIME pasado como parámetro
	 *
	 * @param contentType ...
	 * @return Default extension ...
	 */
	public String getDefaultExtension(String contentType) {
		try {
			TikaConfig config = TikaConfig.getDefaultConfig();
			MimeType mimeType = config.getMimeRepository().forName(contentType);
			return mimeType.getExtension();
		} catch (Exception e) {
			log.error("<getDefaultExtension>: Error getting extension for " + contentType + ": " + e.toString());
			return ".jpg";
		}
	}

	/**
	 * Get the SHA-1 representation of the given String
	 */
	public static String getSHA1(String string) {
		return DigestUtils.sha1Hex(string);
	}

	/**
	 * 
	 * @param text
	 *            to clean/remove illegal chars...
	 * @return
	 */
	public static String cleanChars(String text) {
		String newText = Normalizer.normalize(text, Normalizer.Form.NFD);
		newText = newText.replaceAll("[^\\p{ASCII}]", "");
		newText = newText.replaceAll("[^a-zA-Z0-9-_\\.]", "_");
		return newText;
	}

	/**
	 * Get a given parameter value from a standard web Query String...
	 * 
	 * @param queryString
	 * @param param
	 * @return
	 */
	public static String getParamValue(String queryString, String param) {
		HashMap<String, String> params = getQueryMap(queryString);
		return params.get(param);
	}

	private static HashMap<String, String> getQueryMap(String query) {
		String[] params = query.split("&");
		HashMap<String, String> map = new HashMap<String, String>();
		for (String param : params) {
			String[] p = param.split("=");
			String name = p[0];
			if (p.length > 1) {
				String value = p[1];
				map.put(name, value);
			}
		}
		return map;
	}

	/**
	 * Redondeo a dos decimales (no existe por defecto en Math...)
	 * 
	 * @param d
	 * @return
	 */
	public static int roundZeroDecimals(double d) throws NumberFormatException {
		DecimalFormat zeroDForm = new DecimalFormat("#");
		return Integer.valueOf(zeroDForm.format(d));
	}

	/**
	 * Redondeo a dos decimales (no existe por defecto en Math...)
	 * 
	 * @param d
	 * @return
	 */
	public static double roundTwoDecimals(double d) throws NumberFormatException {
		DecimalFormat twoDForm = new DecimalFormat("#.##");
		return Double.valueOf(twoDForm.format(d));
	}

	/**
	 * Parse and return custom attribute (from extra fields, attribs, ...)
	 *
	 * @param attribs with attributes separated by &
	 * @param attrib to find
	 * @return Value
	 */
	public static String getCustomAttribute(String attribs, String attrib) {
		HashMap<String, String> mapAttributes = new HashMap<>();
		try {
			for (String pair : attribs.split("&")) {
				int idx = pair.indexOf("=");
				try {
					mapAttributes.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
				} catch (Exception ignore) {}
			}
			return mapAttributes.get(attrib);
		} catch (Exception ignore) {
			return "";
		}
	}

	/**
	 * Removes an attribute from a string of attributes... (from extra fields, attribs, ...)
	 *
	 * @param attribs with attributes separated by &
	 * @param attrib to find and remove ..
	 */
	public static String removeCustomAttribute(String attribs, String attrib) {
		HashMap<String, String> mapAttributes = new HashMap<>();
		try {
			for (String pair : attribs.split("&")) {
				int idx = pair.indexOf("=");
				try {
					String key = URLDecoder.decode(pair.substring(0, idx), "UTF-8");
					String value = URLDecoder.decode(pair.substring(idx + 1), "UTF-8");
					System.out.println("key = " + key + " -- value = " + value);
					if (!attrib.equals(key)) {
						System.out.println("Adding key " + key);
						mapAttributes.put(key, value);
					}
				} catch (Exception ignore) {}
			}
			// Regenerate attribs....
			Iterator it = mapAttributes.entrySet().iterator();
			StringBuilder params = new StringBuilder();
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry) it.next();
				params.append(pair.getKey()).append("=").append(pair.getValue()).append("&");
				it.remove(); // avoids a ConcurrentModificationException
			}
			attribs = params.toString();
			// Removes last &
			attribs = attribs.substring(0, attribs.length()-1);
			return attribs;
		} catch (Exception ignore) {
			return attribs;
		}
	}

	/**
	 * StackTrace al Log4Java...
	 *
	 * @param e Excepeción de turno...
	 */
	public static void logStrackTrace(Exception e) {
		log.error("----------  Begin StackTrace  --------------------------------");
		StackTraceElement[] stackTraceElements = e.getStackTrace();
		for (StackTraceElement stackTrace : stackTraceElements) {
			log.error(stackTrace.getClassName() + "  " + stackTrace.getMethodName()
					+ " " + stackTrace.getLineNumber());
		}
		log.error("----------  End StackTrace  ----------------------------------");
	}

}