package com.gruposm.backend.utils.thymeleaf;

import com.gruposm.backend.utils.Config;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.FileTemplateResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;

import javax.servlet.ServletContext;

/**
 * Store and retrieves Thymeleaf TemplateEngine into the application servlet context.
 */
public class TemplateEngineUtil {

	/** Types of Templates */
	public static final String TEMPLATE_ADMIN = "template_admin";
	public static final String TEMPLATE_MAIN = "template_main";

    public static void storeTemplateEngine(ServletContext context) {
    	TemplateEngine engineAdmin = buildTemplateEngine(context, TEMPLATE_ADMIN);
    	TemplateEngine engineMain = buildTemplateEngine(context, TEMPLATE_MAIN);
        context.setAttribute(TEMPLATE_ADMIN, engineAdmin);
        context.setAttribute(TEMPLATE_MAIN, engineMain);
    }
    
    public static TemplateEngine getTemplateEngine(ServletContext context, String templateType) {
        TemplateEngine engine = (TemplateEngine) context.getAttribute(templateType);
        // HotFix: Jetty 9.4.6.... creación bajo demanda...
        if (engine == null) {
            // Creamos bajo demanda....
            switch (templateType) {
                case TEMPLATE_ADMIN:
                    TemplateEngine engineMgmt = buildTemplateEngine(context, TEMPLATE_ADMIN);
                    context.setAttribute(TEMPLATE_ADMIN, engineMgmt);
                    break;
                case TEMPLATE_MAIN:
                    TemplateEngine engineSlatebox = buildTemplateEngine(context, TEMPLATE_MAIN);
                    context.setAttribute(TEMPLATE_MAIN, engineSlatebox);
                    break;
            }
            // Return it...
            engine = (TemplateEngine) context.getAttribute(templateType);
        }
    	return engine;
    }

    private static TemplateEngine buildTemplateEngine(ServletContext servletContext, String templateType) {
        TemplateEngine engine = new TemplateEngine();
       	engine.setTemplateResolver(getTemplateResolver(servletContext, templateType));
        return engine;
    }

    private static ITemplateResolver getTemplateResolver(ServletContext servletContext, String templateType) {
    	FileTemplateResolver resolver = new FileTemplateResolver();
    	String prefix = "";
    	switch (templateType) {
            case TEMPLATE_ADMIN:
                prefix = Config.getWebPrefixPath() + Config.getWebAdminPath();
                break;
            case TEMPLATE_MAIN:
                prefix = Config.getWebPrefixPath() + Config.getWebMainPath();
                break;
        }
        resolver.setPrefix(prefix);
        resolver.setSuffix(".html");
        resolver.setCharacterEncoding("UTF-8"); 
        resolver.setTemplateMode(TemplateMode.HTML);
        resolver.setCacheable(Config.getThymeleafCache());
        return resolver;
    }
    
    /**
     * Ad-hoc, without ServletContext and the like....
     * 
     * @return
     */
    public static TemplateEngine getOfflineTemplateEngine() {
    	ITemplateResolver resolver = getTemplateResolver(null, TEMPLATE_MAIN);
    	TemplateEngine engine = new TemplateEngine();
    	engine.setTemplateResolver(resolver);
    	return engine;
    }
}