package com.gruposm.backend.domain;

import java.io.Serializable;
import java.util.Objects;

/**
 * A helper class for AppCode
 * 
 * @version 0.1
 * @date Nov 14, 2016
 * @author Jordi G. March <jordi.garcia@colex.grupo-sm.com>
 */
public class AppCode implements Serializable {

	private static final long serialVersionUID = 1L;

	/** Atributos */
	private String uuidCode;
	private int idApp;
	private long createdOn;
	private long usedOn;
	private String usedBy;

	public AppCode() {
		
	}

	public String getUuidCode() {
		return uuidCode;
	}

	public void setUuidCode(String uuidCode) {
		this.uuidCode = uuidCode;
	}

	public int getIdApp() {
		return idApp;
	}

	public void setIdApp(int idApp) {
		this.idApp = idApp;
	}

	public long getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(long createdOn) {
		this.createdOn = createdOn;
	}

	public long getUsedOn() {
		return usedOn;
	}

	public void setUsedOn(long usedOn) {
		this.usedOn = usedOn;
	}

	public String getUsedBy() {
		return usedBy;
	}

	public void setUsedBy(String usedBy) {
		this.usedBy = usedBy;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		AppCode appCode = (AppCode) o;
		return Objects.equals(uuidCode, appCode.uuidCode);
	}

	@Override
	public int hashCode() {

		return Objects.hash(uuidCode);
	}

	@Override
	public String toString() {
		return "AppCode{" +
				"uuidCode='" + uuidCode + '\'' +
				", idApp=" + idApp +
				", createdOn=" + createdOn +
				", usedOn=" + usedOn +
				", usedBy='" + usedBy + '\'' +
				'}';
	}
}
