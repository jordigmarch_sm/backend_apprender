package com.gruposm.backend.domain;

import java.io.Serializable;
import java.util.Objects;

/**
 * A helper class for Apps
 * 
 * @version 0.1
 * @date Nov 14, 2016
 * @author Jordi G. March <jordi.garcia@colex.grupo-sm.com>
 */
public class App implements Serializable {

	private static final long serialVersionUID = 1L;

	/** Atributos */
	private int id;
	private String title;
	private String description;
	private long updatedOn;
	private String tenant;

	public App() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(long updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getTenant() {
		return tenant;
	}

	public void setTenant(String tenant) {
		this.tenant = tenant;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		App app = (App) o;
		return id == app.id;
	}

	@Override
	public int hashCode() {

		return Objects.hash(id);
	}

	@Override
	public String toString() {
		return "App{" +
				"id=" + id +
				", title='" + title + '\'' +
				", description='" + description + '\'' +
				", updatedOn=" + updatedOn +
				", tenant='" + tenant + '\'' +
				'}';
	}
}
