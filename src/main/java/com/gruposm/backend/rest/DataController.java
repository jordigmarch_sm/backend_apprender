package com.gruposm.backend.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gruposm.backend.domain.AppCode;
import com.gruposm.backend.exception.AlreadyUsedException;
import com.gruposm.backend.exception.AuthException;
import com.gruposm.backend.exception.CookieException;
import com.gruposm.backend.exception.NotFoundException;
import com.gruposm.backend.persistence.ConnectionFactory;
import com.gruposm.backend.service.AppCodeService;
import com.gruposm.backend.utils.AuthUtils;
import com.gruposm.backend.utils.Config;
import org.apache.log4j.Logger;
import org.eclipse.jetty.http.HttpStatus;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


/**
 * REST Data Layer
 * <p>
 * ...
 * 
 * 
 * @version 0.1
 * @date Jun 23, 2016
 * @author Jordi G. March <jordi.garcia@colex.grupo-sm.com>
 */
@Path("/")
public class DataController {

	/** Log4Java */
	private static final Logger log = Logger.getLogger(DataController.class);
	
	@GET
	@Path("/verify")
	@Produces(MediaType.TEXT_PLAIN)
	public Response verifyRESTService() {
		String result = "RESTService Successfully started..";
 
		// return HTTP response 200 in case of success
		return Response.status(200).entity(result).build();
	}
	
	@GET
	@Path("/clearCache/{token}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response clearCache(@PathParam("token") String token) {
		if (Config.getAdminToken().equals(token)) {
			ConnectionFactory.clearCache();
			String result = "MyBatis cache cleared successfully!";
			return Response.status(HttpStatus.OK_200).entity(result).build();			
		} else {
			String error = "A security token is needed to perform administrative tasks";
			return Response.status(HttpStatus.UNAUTHORIZED_401).entity(error).build();
		}
	}
	
	/**
	 * Get a previously saved uuid.
	 * <p>
	 * Must validate against AuthUtils with a valid sessionId in case Session is Required 
	 * at Config level...
	 * 
	 * @return JSON with the associated MainEntity
	 */
	@GET
	@Path("/check")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@QueryParam("id") String id,
						@QueryParam("user") String userId,
						@QueryParam("token") String token,
						@Context HttpHeaders headers) {
		try {
			ObjectMapper mapper = new ObjectMapper();

			// Check security synchronously...
			if (Config.isTokenSecurityEnabled()) {
				AuthUtils.getInstance().validateToken(id, token);
			}

			AppCodeService as = new AppCodeService();
			AppCode appCode = as.getBy(id, userId);
			if (userId != null && !userId.trim().equals("")) {
				appCode.setUsedBy(userId);
			}
			as.update(appCode);
			return Response.status(HttpStatus.OK_200).entity(mapper.writeValueAsString(appCode)).build();
		} catch (AuthException | AlreadyUsedException e) {
			log.error("<check>: Unauthorized access: " + e.getMessage());
			String jsonError = "{\"error\":\"" + e.getMessage() + "\"}";
			return Response.status(HttpStatus.UNAUTHORIZED_401).entity(jsonError).build();
		} catch (NotFoundException e) {
			log.error("<check>: Error saving JSON: " + e.toString());
			String jsonError = "{\"error\":\"" + e.getMessage() + "\"}";
			return Response.status(HttpStatus.NOT_FOUND_404).entity(jsonError).build();
		} catch (Exception e) {
		    if (!e.toString().contains(AuthException.class.getName()) &&
				!e.toString().contains(CookieException.class.getName())) {
		        e.printStackTrace();
            }
			log.error("<check>: Error saving JSON: " + e.toString());
			String jsonError = "{\"error\":\"" + e.getMessage() + "\"}";
			return Response.status(HttpStatus.INTERNAL_SERVER_ERROR_500).entity(jsonError).build();
		}
	}

	/**
	 * Gets default JSON languages...
	 * <p>
	 * Must validate against AuthUtils with a valid sessionId in case Session is Required
	 * at Config level...
	 *
	 * @return default JSON under assets/json/...
	 */
	@GET
	@Path("/version")
	@Produces(MediaType.APPLICATION_JSON)
	public Response version(@QueryParam("code") String code,
							@QueryParam("token") String token,
							@Context HttpHeaders headers) {
		return versionfile(Config.getDefaultAssetsJsonFile(), code, token, headers);
	}

	/**
	 * Get a JSON config file specified by json parameter...
	 * <p>
	 * Must validate against AuthUtils with a valid sessionId in case Session is Required
	 * at Config level...
	 *
	 * @return JSON ...
	 */
	@GET
	@Path("/versionfile")
	@Produces(MediaType.APPLICATION_JSON)
	public Response versionfile(@QueryParam("json") String json,
							@QueryParam("code") String code,
							@QueryParam("token") String token,
							@Context HttpHeaders headers) {
		try {
			ObjectMapper mapper = new ObjectMapper();

			// Check security synchronously...
			if (Config.isTokenSecurityEnabled()) {
				AuthUtils.getInstance().validateToken(code, token);
			}

			AppCodeService as = new AppCodeService();
			String jsonToReturn = as.getLastVersion(json);
			return Response.status(HttpStatus.OK_200).entity(jsonToReturn).build();
		} catch (AuthException | AlreadyUsedException e) {
			log.error("<check>: Unauthorized access: " + e.getMessage());
			String jsonError = "{\"error\":\"" + e.getMessage() + "\"}";
			return Response.status(HttpStatus.UNAUTHORIZED_401).entity(jsonError).build();
		} catch (NotFoundException e) {
			log.error("<check>: Error saving JSON: " + e.toString());
			String jsonError = "{\"error\":\"" + e.getMessage() + "\"}";
			return Response.status(HttpStatus.NOT_FOUND_404).entity(jsonError).build();
		} catch (Exception e) {
			if (!e.toString().contains(AuthException.class.getName()) &&
					!e.toString().contains(CookieException.class.getName())) {
				e.printStackTrace();
			}
			log.error("<check>: Error saving JSON: " + e.toString());
			String jsonError = "{\"error\":\"" + e.getMessage() + "\"}";
			return Response.status(HttpStatus.INTERNAL_SERVER_ERROR_500).entity(jsonError).build();
		}
	}
	
}
