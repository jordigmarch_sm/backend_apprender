package com.gruposm.backend.service.session;

import com.gruposm.backend.domain.session.Session;
import com.gruposm.backend.persistence.mappers.session.SessionMapper;
import com.gruposm.backend.service.AbstractService;
import com.gruposm.backend.utils.Constants;
import com.gruposm.backend.utils.Utils;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionManager;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.List;

/**
 * Service Class for Sessions/Authorization
 * 
 * @version 0.1
 * @date Nov 15, 2016
 * @author Jordi G. March <jordi.garcia@colex.grupo-sm.com>
 */
public class SessionService extends AbstractService {

	/** Log4Java */
	private static final Logger log = Logger.getLogger(SessionService.class);
	
	public SessionService() {	
		super();
	}
	public SessionService(SqlSessionManager sessionManager) {
		super(sessionManager);
	}
	
	/**
	 * Default Insert method, based on <code>userId</code>
	 * 
	 * @param userId user
	 * @param attributes queryString
	 */
	public String insert(String userId, String attributes, String tenant) {
		SqlSession sqlSession = getSession();
		String sessionId = null;
		try {
			SessionMapper sessionMapper = sqlSession.getMapper(SessionMapper.class);
			Session session = new Session();
			long now = System.currentTimeMillis();
			sessionId = Utils.getSHA1(userId + now);
			session.setSession(sessionId);
			session.setUserId(userId);
			session.setCreatedOn(now);
			session.setUpdatedOn(now);
			String deviceId = "";
			if (attributes.contains(Constants.DEVICE_ID)) {
				deviceId = Utils.getCustomAttribute(attributes, Constants.DEVICE_ID);
				attributes = Utils.removeCustomAttribute(attributes, Constants.DEVICE_ID);
			}
			session.setAttributes(attributes);
			session.setDeviceId(deviceId);
			session.setTenant(tenant);
			sessionMapper.insert(session);
		} finally {
			closeSession(sqlSession);
		}
		return sessionId;
	}
	
	/**
	 * Update session.... (updated_on -> NOW())
	 * @param session
	 */
	public void update(Session session) {
		SqlSession sqlSession = getSession();
		try {
			SessionMapper sessionMapper = sqlSession.getMapper(SessionMapper.class);
			sessionMapper.update(session);
		} finally {
			closeSession(sqlSession);
		}
	}
	
	/**
	 * Update attributes of a session, needed whenever a uuid changes completely
	 * (load template, edit template, load previously saved uuid, etc...).
	 * <p>
	 * In order to allow full browser refreshes and the like, we need to maintain 
	 * coherence at session attributes level. So, if a new uuid is loaded,
	 * the attributes of the session attached to the current logged user must be
	 * changed honoring the new uuid at the attributes field of session table.
	 * 
	 * @param session
	 * @param attribute
	 * @param value
	 */
	public void updateAttributes(Session session, String attribute, String value) {
		String attrs = session.getAttributes();
		String oldAttribute = Utils.getParamValue(attrs, attribute);
		String newAttrs = attrs.replace(oldAttribute, value);
		session.setAttributes(newAttrs);
		update(session);
	}
	
	/**
	 * Get All Sessions currently active
	 * 
	 */
	public List<Session> getAll() {
		SqlSession sqlSession = getSession();
		try {
			SessionMapper sessionMapper = sqlSession.getMapper(SessionMapper.class);
			return sessionMapper.getAll();
		} finally {
			closeSession(sqlSession);
		}
	}
	
	/**
	 * Get Session based on sessionId
	 * 
	 * @param id sessionId to search by...
	 */
	public List<Session> getSessionsById(String id) {
		SqlSession sqlSession = getSession();
		try {
			SessionMapper sessionMapper = sqlSession.getMapper(SessionMapper.class);
			return sessionMapper.getById(id);
		} finally {
			closeSession(sqlSession);
		}
	}
	
	/**
	 * Get Session based on sessionId
	 * 
	 * @param id sessionId to search by...
	 */
	public Session getById(String id) {
		SqlSession sqlSession = getSession();
		try {
			SessionMapper sessionMapper = sqlSession.getMapper(SessionMapper.class);
			List<Session> listSessions = sessionMapper.getById(id);
			Session session = null;
			if (listSessions.size() > 0) {
				session = listSessions.get(0);
				return session;
			} else {
				return null;
			}
		} finally {
			closeSession(sqlSession);
		}
	}
	
	/**
	 * Default...
	 * @return
	 */
	public void remove(Session session) {
		SqlSession sqlSession = getSession();
		try {
			SessionMapper sessionMapper = sqlSession.getMapper(SessionMapper.class);
			sessionMapper.remove(session);
		} finally {
			closeSession(sqlSession);
		}
	}

	/**
	 * Remove sessions where (now() - updated_on) > minutes 
	 * @param minutes
	 */
	public void removeSessionsAfter(int minutes) {
		SqlSession sqlSession = getSession();
		try {
			SessionMapper sessionMapper = sqlSession.getMapper(SessionMapper.class);
			// sessionMapper.removeSessionsAfter(minutes);
			HashMap<String, Object> map = new HashMap<>();
			map.put("attributes", "%" + Constants.FROM_FIREBASE + "%");
			map.put("minutes", minutes);
			sessionMapper.removeNormalSessionsAfter(map);
		} finally {
			closeSession(sqlSession);
		}
	}

	public void updateJustTimestamp(Session session) {
		SqlSession sqlSession = getSession();
		try {
			SessionMapper sessionMapper = sqlSession.getMapper(SessionMapper.class);
			sessionMapper.updateJustTimestamp(session);
		} finally {
			closeSession(sqlSession);
		}
	}

    public Session getByUserId(String userId, String tenant) {
		SqlSession sqlSession = getSession();
		try {
			SessionMapper sessionMapper = sqlSession.getMapper(SessionMapper.class);
			HashMap<String, String> map = new HashMap<>();
			map.put(Constants.USER_ID, userId);
			map.put(Constants.TENANT, tenant);
			return sessionMapper.getByUserId(map);
		} finally {
			closeSession(sqlSession);
		}
    }

	public Session getByDeviceId(String deviceId) {
		SqlSession sqlSession = getSession();
		try {
			SessionMapper sessionMapper = sqlSession.getMapper(SessionMapper.class);
			List<Session> listSessions = sessionMapper.getByDeviceId(deviceId);
			Session session = null;
			if (listSessions.size() > 0) {
				session = listSessions.get(0);
				return session;
			} else {
				return null;
			}
		} finally {
			closeSession(sqlSession);
		}
	}
}