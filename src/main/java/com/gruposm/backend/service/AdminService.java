package com.gruposm.backend.service;

import com.gruposm.backend.domain.Admin;
import com.gruposm.backend.persistence.mappers.AdminMapper;
import com.gruposm.backend.utils.Constants;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionManager;

import java.util.HashMap;
import java.util.List;

/**
 * Service Class for Sessions/Authorization
 * 
 * @version 0.1
 * @date Nov 15, 2016
 * @author Jordi G. March <jordi.garcia@colex.grupo-sm.com>
 */
public class AdminService extends AbstractService {

	public AdminService() {	
		super();
	}
	public AdminService(SqlSessionManager sessionManager) {
		super(sessionManager);
	}
	
	
	/**
	 * Default Insert method, based on <code>userId</code>
	 * 
	 * @param admin Admin object
	 */
	public void insert(Admin admin) {
		SqlSession sqlSession = getSession();
		try {
			AdminMapper adminMapper = sqlSession.getMapper(AdminMapper.class);
			adminMapper.insert(admin);
		} finally {
			closeSession(sqlSession);
		}
	}
	
	/**
	 * Update admin.... 
	 * @param admin ...
	 */
	public void update(Admin admin) {
		SqlSession sqlSession = getSession();
		try {
			AdminMapper adminMapper = sqlSession.getMapper(AdminMapper.class);
			adminMapper.update(admin);
		} finally {
			closeSession(sqlSession);
		}
	}
	
	/**
	 * Get All Sessions currently active
	 * 
	 */
	public List<Admin> getAll() {
		SqlSession sqlSession = getSession();
		try {
			AdminMapper adminMapper = sqlSession.getMapper(AdminMapper.class);
			return adminMapper.getAll();
		} finally {
			closeSession(sqlSession);
		}
	}
	
	/**
	 * Get Session based on userId
	 * 
	 * @param username userId
	 */
	public Admin getByName(String username, String tenant) {
		SqlSession sqlSession = getSession();
		try {
			AdminMapper adminMapper = sqlSession.getMapper(AdminMapper.class);
			HashMap<String, String> map = new HashMap<>();
			map.put(Constants.USERNAME, username);
			map.put(Constants.TENANT, tenant);
			return adminMapper.getByName(map);
		} finally {
			closeSession(sqlSession);
		}
	}
	
	/**
	 * Default...
	 * @return
	 */
	public void remove(Admin admin) {
		SqlSession sqlSession = getSession();
		try {
			AdminMapper adminMapper = sqlSession.getMapper(AdminMapper.class);
			adminMapper.remove(admin);
		} finally {
			closeSession(sqlSession);
		}
	}
	
}