package com.gruposm.backend.service.jobs;

import com.gruposm.backend.persistence.ConnectionFactory;
import com.gruposm.backend.service.ControllerService;
import com.gruposm.backend.service.session.SessionService;
import com.gruposm.backend.utils.Config;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;

import java.nio.charset.StandardCharsets;


/**
 * Clean sessions that may have been orphaned...
 * 
 * @version 0.1
 * @date Nov 15, 2016
 * @author Jordi G. March <jordi.garcia@colex.grupo-sm.com>
 */
public class CleanSessionsJob implements Job
{
	/** Log4Java */
	private static Logger log = Logger.getLogger(CleanSessionsJob.class);

	/** Secheduler Quartz - in case we need it... */
	private Scheduler scheduler;
	
	/** SQLSession MyBatis object */
	private SqlSession sqlSession;
	
	/** UTF-8 */
	private static final String UTF8 = StandardCharsets.UTF_8.name();
	
	/**
	 * @see ControllerService
	 */
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException 
	{
		log.info("<execute>: CleanSessionsJob started!");
		long ini = System.currentTimeMillis();
		SqlSession sqlSession = ConnectionFactory.getSession().openSession();
		scheduler = ControllerService.getInstance().getScheduler();
		try {
			// Sessions
			doSessionsJob();

			long end = System.currentTimeMillis() - ini;
			log.info("<execute>: CleanSessionsJob finished correctly in " + end + " ms.");
		} catch (Exception e) {
			log.error("<execute>: Error running CleanSessionsJob: " + e.toString());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
	}

	/**
	 * Clean Sessions itself...
	 * <p>
	 * Run it in case AuthUtils.SessionCleaner is dead....
	 * 
	 * @throws Exception
	 */
	private void doSessionsJob() throws Exception {
		SessionService ss = new SessionService();
		ss.removeSessionsAfter(Config.getSessionTimeout());
	}

	public static void main(String args[]) throws Exception {
		CleanSessionsJob kk = new CleanSessionsJob();
		kk.execute(null);
	}
}
