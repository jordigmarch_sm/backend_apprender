package com.gruposm.backend.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gruposm.backend.persistence.ConnectionFactory;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionManager;
import org.apache.ibatis.session.TransactionIsolationLevel;

/**
 * Abstract Service Class, in order to deal with transactions and general things...
 * 
 * @version 0.1
 * @date Dec 21, 2016
 * @author Jordi G. March <jordi.garcia@colex.grupo-sm.com>
 */
public abstract class AbstractService {

	/** SQLSessionManager for transactional purposes */
	protected SqlSessionManager sessionManager;

	/** Mapper to JSON */
	protected ObjectMapper mapper;
	
	/** Transaction active? */
	protected boolean isTransactionActive;
	
	
	protected AbstractService() {
		
	}
	
	protected AbstractService(SqlSessionManager sessionManager) {
		this.sessionManager = sessionManager;
	}

	protected SqlSession getSession() {
		// By default REPEATABLE_READ -> InnoDB y esas gaitas...
		return getSession(TransactionIsolationLevel.REPEATABLE_READ);
	}
	
	protected SqlSession getSession(TransactionIsolationLevel serializable) {
		if (sessionManager == null) {
			isTransactionActive = false;
			return ConnectionFactory.getSession().openSession(serializable);	
		} else {
			isTransactionActive = true;
			// TransactionIsolationLevel must be done at sessionManager level, like this:
			// sessionManager.startManagedSession(TransactionIsolationLevel.SERIALIZABLE);
			return sessionManager;
		}
	}
	
	protected void closeSession(SqlSession sqlSession) {
		if (!isTransactionActive) {
			// sqlSession.commit(); // Needed??
			sqlSession.close();
		}		
	}
}