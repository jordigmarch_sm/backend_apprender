package com.gruposm.backend.exception;

public class CookieException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3674248640719955735L;
	
	/**
     *
     */
    public CookieException()
    {
    	super();
    }
    
	/**
     * Constructor with a detail message.
     *
     * @param s the detail message
     */
    public CookieException(String s)
    {
    	super(s);
    }
}
