package com.gruposm.backend.exception;

import org.apache.log4j.Logger;

import com.gruposm.backend.utils.AuthUtils;

public class AuthException extends BackendException {

	/** */
	private static final long serialVersionUID = 3674248640719955735L;

	/** Log4Java */
	private static final Logger log = Logger.getLogger(AuthUtils.class);

	/**
     *
     */
    public AuthException()
    {
    	super();
    }
    
	/**
     * Constructor with a detail message.
     *
     * @param s the detail message
     */
    public AuthException(String s)
    {
    	super(s);
    	log.warn("<AuthException>: " + s);
    }

}
