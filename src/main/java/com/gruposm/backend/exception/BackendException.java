package com.gruposm.backend.exception;

/**
 * Backend General Exception
 * 
 * @version 0.1
 * @date Jun 24, 2016
 * @author Jordi G. March <jordi.garcia@colex.grupo-sm.com>
 */
public class BackendException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1840451845639572519L;
	
	/**
	 *
     */
    public BackendException()
    {
    	super();
    }
    
	/**
     * Constructor with a detail message.
     *
     * @param s the detail message
     */
    public BackendException(String s)
    {
    	super(s);
    }
}
